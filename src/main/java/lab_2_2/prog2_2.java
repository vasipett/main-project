package lab_2_2;

import static java.lang.System.out;

class Animal
{
	//instance field
	String food;
	String location;

	//constructors
	Animal(String newFood, String newLocation)
	{
		this.food = newFood;
		this.location = newLocation;
	}

	//methods
	public static void makeNoise()
	{

	}

	public static void eat()
	{

	}

	public static void sleep(Animal myAnimal)
	{
		out.println(myAnimal.getClass().getName() + " sleeping...");
	}
}

class Dog extends Animal
{
	//instance field
	static
	{
		int countOfLegs;
	}

	//constructors
	Dog(String f1, String l1)
	{
		super(f1, l1);
	}

	//methods
	public static void eat()
	{

	}

	public static void makeNoise()
	{

	}
}

class Cat extends Animal
{
	//instance field
	static
	{
		int countOfeyes;
	}

	//constructors
	Cat(String f2, String l2)
	{
		super(f2, l2);
	}

	//methods
	public static void eat()
	{

	}

	public static void makeNoise()
	{

	}
}

class Horse extends Animal
{
	//instance field
	static
	{
		boolean isAlive;
	}

	//constructors
	Horse(String f3, String l3)
	{
		super(f3, l3);
	}

	//methods
	public static void eat()
	{

	}

	public static void makeNoise()
	{

	}
}

class Veterinarian
{
	//methods
	public static void treatAnimal(Animal animal)
	{
		String aFood = animal.food;
		String aLocation = animal.location;

		out.println( "\n name: " + animal.getClass().getName() + "\n food: " + aFood + "\n location: " + aLocation );
	}
}

public class prog2_2
{
	public static void main(String args[])
	{
		Veterinarian doctor = new Veterinarian();
		Animal[] animalArray = new Animal[3];

		animalArray[0] = new Dog("meat", "outside");
		animalArray[1] = new Cat("milk", "living room");
		animalArray[2] = new Horse("hay", "garage");

		for(Animal e : animalArray)
			doctor.treatAnimal(e);
	}
}
