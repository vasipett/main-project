package lab_2_4;

import static java.lang.System.out;

interface Shape
{
	int PI = 3;
	int square();
}

class Circle implements Shape
{
	//instance field
	int radius;

	//constructors
	Circle(int aRadius)
	{
		this.radius = aRadius;
	}

	//methods
	public int square()
	{
		return PI * this.radius * this.radius;
	}
}

class Triangle implements Shape
{
	//instance field
	int base;
	int height;

	//constructors
	Triangle(int aBase, int aHeight)
	{
		this.base = aBase;
		this.height = aHeight;
	}

	//methods
	public int square()
	{
		return this.base * this.height / 2;
	}
}

class Rectangle implements Shape
{
	//instance field
	int side1;
	int side2;

	//constructors
	Rectangle(int aSide1, int aSide2)
	{
		this.side1 = aSide1;
		this.side2 = aSide2;
	}

	//methods
	public int square()
	{
		return this.side1 * this.side2;
	}
}

public class prog2_4
{
	public static void main(String args[])
	{
		Shape[] shape = new Shape[3];

		shape[0] = new Triangle(2, 3);
		shape[1] = new Circle(2);
		shape[2] = new Rectangle(3, 5);

		for(Shape sh : shape)
			out.println("figure " + sh.getClass().getName() + " area: " + sh.square());
	}
}

