package lab_2_1;

import java.util.Scanner;

class Phone
{
	//instance field
	int number;
	String model;
	int weight;

	//constructors
	public Phone(String variable1)
	{

	}

	public Phone(int number1, String model1, int weight1)
	{
		this(12, "Asus");
		this.number = number1;
		this.model = model1;
		this.weight = weight1;
	}

	public Phone(int number2, String model2)
	{

	}

	public Phone()
	{

	}

	//methods
	public void receiveCall(String callerName)
	{
		System.out.println("Calling " + callerName);
	}

	public void receiveCall(String name, int number)
	{
		System.out.println("hi");
	}

	public int getNumber()
	{
		return number;
	}

	public void sendMessage(String allNumbers)
	{
		System.out.println(allNumbers);
	}

}

public class prog2_1
{

	public static void main(String args[])
	{
		Phone newPhone = new Phone();
		Phone[] newObjects = new Phone[3];

		for(int i = 0; i < 3; i++)
			newObjects[i] = new Phone(1+i, "Nokla", 200+i*10);

		for(int i = 0; i < 3; i++)
			System.out.println("n: " + newObjects[i].number + "\n m: " +
					newObjects[i].model + "\n w: " +
					newObjects[i].weight);

		for(int i = 0; i < 3; i++)
		{
			newObjects[i].receiveCall("human");
			System.out.println( newObjects[i].getNumber() );
		}


	}
}
