package lab_4_1;

import static java.lang.System.out;
import java.io.*;
import java.util.*;

class NotEnoughMoneyException extends Throwable
{
    public NotEnoughMoneyException()
    {
        super("Error. You don't have enough money. Please increase your balance and try again.");
    }
}

class UnknownAccountException extends Throwable
{
    public UnknownAccountException()
    {
        super("Error. This account doesn't exist. Please create new and try again.");
    }
}

interface AccountServiceAble
{
    public abstract void withdraw(int accountId, int amount)throws
            NotEnoughMoneyException,
            UnknownAccountException;

    public abstract void balance(int accountId) throws
            UnknownAccountException;

    public abstract void deposite(int acountId, int amount) throws
            NotEnoughMoneyException,
            UnknownAccountException;

    public abstract void transfer(int from, int to, int amount) throws
            NotEnoughMoneyException,
            UnknownAccountException;
}

class Account
{
    public int id;
    public String holder;
    public int amount;

    public Account(int id, String holder, int amount)
    {
        this.id = id;
        this.holder = holder;
        this.amount = amount;
    }

    public Account()
    {

    }
}

class AccountService extends Account// implements AccountServiceAble
{
    public static void withdraw(ArrayList dataBase, int accountId, int amount) throws
            NotEnoughMoneyException,
            UnknownAccountException
    {
        int intBuffer = searchById(dataBase, accountId);
        if(intBuffer != -1)
        {
            Account AccountBuffer = (Account)dataBase.get(intBuffer);
            if(amount > 0 && AccountBuffer.amount >= amount)
                AccountBuffer.amount -= amount;
            else
                throw new NotEnoughMoneyException();
        }
        else
            throw new UnknownAccountException();

    }

    public static void balance(ArrayList dataBase, int accountId) throws
            UnknownAccountException
    {
        int intBuffer = searchById(dataBase, accountId);
        if(intBuffer != -1)
        {
            Account AccountBuffer = (Account)dataBase.get(intBuffer);
            out.println("holder: " + AccountBuffer.holder + "\namount: " + AccountBuffer.amount);

        }
        else
            throw new UnknownAccountException();
    }

    public static void deposite(ArrayList dataBase, int accountId, int amount) throws
            UnknownAccountException
    {
        int intBuffer = searchById(dataBase, accountId);
        if(intBuffer != -1)
        {
            Account AccountBuffer = (Account)dataBase.get(intBuffer);
            if(amount > 0)
                AccountBuffer.amount += amount;
        }
        else
            throw new UnknownAccountException();
    }

    public static void transfer(ArrayList dataBase, int accountIdFrom, int accountIdTo, int amount) throws
            NotEnoughMoneyException,
            UnknownAccountException
    {
        try
        {
            withdraw(dataBase, accountIdFrom, amount);
            deposite(dataBase, accountIdTo, amount);
        }
        catch (UnknownAccountException e3)
        {
            throw e3;
        }
        catch (NotEnoughMoneyException e4)
        {
            throw e4;
        }
    }

    public static int getAmount(String myString) throws
            UnknownAccountException
    {
        String[] stringBuffer = myString.split(":");

        return Integer.parseInt(stringBuffer[2]);
    }

    public static String getName(String myString) throws
            UnknownAccountException
    {
        String[] stringBuffer = myString.split(":");

        return stringBuffer[1];
    }

    public static int getId(String myString) throws
            UnknownAccountException
    {
        String[] stringBuffer = myString.split(":");

        return Integer.parseInt(stringBuffer[0]);
    }

    public static int searchById(ArrayList dataBase, int accountId)
    {
        int i = 0;
        boolean flag = false;

        while(i < dataBase.size() && flag == false)
        {
            Account accountBuffer = (Account)dataBase.get(i);
            flag = (accountBuffer.id == accountId);
            i++;
        }
        if(flag)
        {
            return i - 1;
        }
        else
            return -1;
    }

    public static int read(int value)
    {
        int variable;

        Scanner in = new Scanner(System.in);
        variable = in.nextInt();

        return variable;
    }

    public static String read(String value)
    {
        String variable;

        Scanner in = new Scanner(System.in);
        variable = in.nextLine();

        return variable;
    }

    public static void request(ArrayList dataBase, String exitLiteral) throws
            UnknownAccountException
            ,NotEnoughMoneyException
    {
        String command = "";

        while(!command.equals(exitLiteral))
        {
            out.print("\n* ");
            command = read(command);

            switch (command)
            {
                case "balance":
                    //balance code
                    int bufferId = -1;
                    try
                    {
                        out.print("enter id: ");
                        balance(dataBase, read(bufferId));
                    }
                    catch(UnknownAccountException e)
                    {
                        throw e;
                    }
                    break;

                case "withdraw":
                    //withdwaw code
                    int bufferId2 = -1;
                    int amount1 = -1;
                    out.print("enter id: ");
                    bufferId2 = read(bufferId2);
                    out.print("enter amount: ");
                    amount1 = read(amount1);
                    try
                    {
                        withdraw(dataBase, bufferId2, amount1);
                        balance(dataBase, bufferId2);
                    }
                    catch(UnknownAccountException e)
                    {
                        throw e;
                    }
                    catch(NotEnoughMoneyException e1)
                    {
                        throw e1;
                    }

                    break;

                case "deposite":
                    //deposit code
                    int bufferId1 = -1;
                    int amount = -1;
                    out.print("enter id: ");
                    bufferId1 = read(bufferId1);
                    out.print("enter amount: ");
                    amount = read(amount);
                    try
                    {
                        deposite(dataBase, bufferId1, amount);
                        balance(dataBase, bufferId1);
                    }
                    catch(UnknownAccountException e)
                    {
                        throw e;
                    }
                    break;

                case "transfer":
                    //transfer code
                    int accountIdFrom = -1;
                    int accountIdTo = -1;
                    int amount3 = -1;
                    out.print("enter id from: ");
                    accountIdFrom = read(accountIdFrom);
                    out.print("enter id to: ");
                    accountIdTo = read(accountIdTo);
                    out.print("enter amount: ");
                    amount3 = read(amount3);
                    try
                    {
                        transfer(dataBase, accountIdFrom, accountIdTo, amount3);
                    }
                    catch(UnknownAccountException e)
                    {
                        throw e;
                    }
                    catch(NotEnoughMoneyException e1)
                    {
                        throw e1;
                    }
                    break;

                case "exit":
                    //exit code
                    out.println("exiting...");
                    break;

                default:
                    out.println("ERROR WHILE READING...");
                    break;
            }
        }

    }

}

class WriteAndOutput
{
    public static void writeToFile(String path, String fileName, boolean cleanPrevious, String args) throws
            FileNotFoundException,
            IOException
    {
        String fullPath = path + "\\" + fileName;
        cleanPrevious = !cleanPrevious;

        try
        {
            FileOutputStream outStream = new FileOutputStream(fullPath, cleanPrevious);

            outStream.write(args.getBytes());//new string

            outStream.write("\n".getBytes());

            outStream.close();
        }
        catch (FileNotFoundException e3)
        {
            File file = new File(path);
            file.mkdirs();

            throw e3;
        }
        catch (IOException e2)
        {
            throw e2;
        }
    }

    public static void printFromFile(String path, String fileName) throws
            FileNotFoundException,
            IOException
    {
        String fullPath = path + "\\" + fileName;

        out.println("path " + fullPath + " contains: ");

        try
        {
            FileInputStream inStream = new FileInputStream(fullPath);
            int i = 0;

            while((i = inStream.read()) != -1)//output file
                out.print((char)i);
        }
        catch (FileNotFoundException e3)
        {
            File file = new File(path);
            file.mkdirs();
            throw e3;
        }
        catch (IOException e4)
        {
            throw e4;
        }
    }

    public static ArrayList readFromFile(String path, String fileName, String[] dataBaseDefault) throws
            FileNotFoundException,
            IOException
    {
        String fullPath = path + "\\" + fileName;
        ArrayList myList = new ArrayList<String>();

        try
        {
            FileInputStream inStream = new FileInputStream(fullPath);
            int i = 0;
            String stringBuffer = "";

            while((i = inStream.read()) != -1)//output file
            {
                if((char)i != '\n')
                    stringBuffer += (char)i;
                else
                {
                    myList.add(stringBuffer);
                    stringBuffer = "";
                }

            }

            return myList;
        }
        catch (FileNotFoundException e3)
        {
            File file = new File(path);
            file.mkdirs();

            for(String str : dataBaseDefault)
                writeToFile(path, fileName, false, str);

            throw e3;
        }
        catch (IOException e4)
        {
            throw e4;
        }
    }

}

public class prog4_1
{
    public static void main(String[] args) throws
            FileNotFoundException
            ,IOException
            ,UnknownAccountException
            ,NotEnoughMoneyException
    {
        try
        {
            String[] dataBaseDefault = {
                    "001:Bob:100"
                    ,"002:Fred:200"
                    ,"003:Rob:300"
                    ,"004:Todd:400"
                    ,"005:Eric:500"
                    ,"006:Dag:600"
                    ,"007:Uri:700"
                    ,"008:Jim:800"
                    ,"009:Bred:900"
                    ,"010:Kyle:1000"};

            WriteAndOutput fileObject = new WriteAndOutput();
            AccountService serviceObject = new AccountService();

            ArrayList myList = new ArrayList<String>();//contains String file data (ID:NAME:AMOUNT)
            ArrayList dataBase = new ArrayList<Account>();//contains file data (ID,Name,Amount)

            ArrayList myListBuffered = fileObject.readFromFile("C:\\Java_code\\save", "beta.txt", dataBaseDefault);//cointains link List

            //push data FFTL (ID:NAME:AMOUNT)
            for(int i = 0; i < myListBuffered.size(); i++)
                myList.add((String)myListBuffered.get(i));

            //push data FLTL (ID,Name,Amount)
            for(int i = 0; i < myList.size(); i++)
            {
                String buffer = (String)myList.get(i);
                dataBase.add(new Account(serviceObject.getId(buffer), serviceObject.getName(buffer), serviceObject.getAmount(buffer)));
            }

            //intermediate data output
            for(int i = 0; i < myList.size(); i++)
            {
                Account buffer1 = (Account)dataBase.get(i);
                out.println(buffer1.id + ", " + buffer1.holder + ", " + buffer1.amount);
            }

            //enter List changes code here
            serviceObject.request(dataBase, "exit");//work with console commands

            //push data FLTL (ID:NAME:AMOUNT)
            for(int i = 0; i < myList.size(); i++)
            {
                Account buffer2 = (Account)dataBase.get(i);
                myList.set(i, buffer2.id + ":" + buffer2.holder + ":" + buffer2.amount);
            }

            //push data FLTF (ID:NAME:AMOUNT)
            for(int i = 0; i < myList.size(); i++)
            {
                if(i == 0)
                    fileObject.writeToFile("C:\\Java_code\\save", "beta.txt", true, (String)myList.get(i));
                else
                    fileObject.writeToFile("C:\\Java_code\\save", "beta.txt", false, (String)myList.get(i));
            }

        }
        catch (FileNotFoundException e1)
        {
            throw e1;
        }
        catch (IOException e2)
        {
            throw e2;
        }
        catch (UnknownAccountException e3)
        {
            throw e3;
        }
        catch (NotEnoughMoneyException e4)
        {
            throw e4;
        }
    }
}
